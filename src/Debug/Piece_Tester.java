package Debug;
import java.util.List;

public class Piece_Tester {
	static Objects.ChessBoard board;
	static Objects.Place[][] places;
	
	static Objects.Bishop bishop;
	static Objects.King king;
	static Objects.Knight knight;
	static Objects.Pawn pawn;
	static Objects.Queen queen;
	static Objects.Rook rook;
	
	public static void main (String args[])
	{
		System.out.println("Row: "+Control.Chess.stringToIndex("d5")[0]+" "+"Column: "+Control.Chess.stringToIndex("d5")[0]);
		
		// Initialize the pieces
		bishop = new Objects.Bishop(true);
		king = new Objects.King(true);
		knight = new Objects.Knight(true);
		pawn = new Objects.Pawn(true);
		queen = new Objects.Queen(true);
		rook = new Objects.Rook(true);
		
		
		// Initialize the Chessboard
		/*
		board = new Objects.ChessBoard(bishop);
		places = board.getPlaces();
		setText(board, bishop.GenerateNextMoves(places[3][3]));
		System.out.print(board);
		*/
		board = new Objects.ChessBoard(king);
		places = board.getPlaces();
		setText(board, king.GenerateNextMoves(places[3][3]));
		System.out.print(board);
		
		board = new Objects.ChessBoard(knight);
		places = board.getPlaces();
		setText(board, knight.GenerateNextMoves(places[3][3]));
		System.out.print(board);
		/*
		board = new Objects.ChessBoard(pawn);
		places = board.getPlaces();
		setText(board, pawn.GenerateNextMoves(places[3][3]));
		System.out.print(board);
		*/
		board = new Objects.ChessBoard(queen);
		places = board.getPlaces();
		setText(board, queen.GenerateNextMoves(places[3][3]));
		System.out.print(board);
		/*
		board = new Objects.ChessBoard(rook);
		places = board.getPlaces();
		setText(board, rook.GenerateNextMoves(places[3][3]));
		System.out.print(board);
		*/
	}
	
	public static void setText (Objects.ChessBoard board, List<Objects.Place> places)
	{
		System.out.println("ArrayList place: "+places);
		for (int r = 7; r >= 0; r--)
		{
			for (int c = 0; c <= 7; c++)
			{
				System.out.println("Checking column: "+c+", row: "+r+".");
				if (places.contains(new Objects.Place(r, c)))
				{
					board.setPlaceText(r, c, "PP");
					System.out.println("HIT!");
				}
			}
		}
	}
}

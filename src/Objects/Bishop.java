package Objects;

import java.util.List;


/**
 * The Bishop class implements abstract class ChessPiece 
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */
public class Bishop extends ChessPiece {
	//fields are inherited from ChessPiece

	public Bishop (boolean isWhite) {
		this.isWhite = isWhite;
	}

	//these methods must be implemented
	/**
	 * Generate list of all possible moves.
	 * @return 	list of all possible moves
	 */
	public List<Place> GenerateNextMoves(Place location, ChessBoard board) {	//calculate all valid moves from current location
		/*
		bR bN bB bQ bK bB bN bR   8
		bp bp bp bp bp bp bp bp   7
		   ##    ##    ##    ##   6
		##    ##    ##    ##      5
		   ##    ##    ##    ##   4
		##    ##    ##    ##      3 
		wp wp wp wp wp wp wp wp   2
		wR wN wB wQ wK wB wN wR   1
		
		 a  b  c  d  e  f  g  h
		
		*  generate an array of places consisting of all the possible locations the piece may be 
		*  moved to on this particular turn. The controller will determine the subset of valid moves
		*/
		

		//A bishop can move diagonally only. Like the rook but more obnoxious
		Place[][] places = board.getPlaces();
		/**
		 * row coordinate
		 */
		int row = location.getRowIndex();
		/**
		 * column coordinate
		 */
		int column = location.getColumnIndex();
		
		//bishops/diagonal movements
		//move forward, right
		for (int i = row, j = column; i < 8; i++, j++) {
			if (j == 8) {
				break;
			}
			
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[i][j].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(i, j));
				break;
			} // ...to here.
			nextMoves.add(new Place(i, j));
		}
		
		//move forward, left
		for (int i = row, j = column; i < 8; i++, j--) {
			if (j < 0) {
				break;
			}
			
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[i][j].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(i, j));
				break;
			} // ...to here.
			
			nextMoves.add(new Place(i, j));
		}
		
		//move backward, right
		for (int i = row, j = column; j < 8; i--, j++) {
			if (i < 0) {
				break;
			}
			
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[i][j].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(i, j));
				break;
			} // ...to here.
			nextMoves.add(new Place(i, j));
		}
		
		//move backward, left
		for (int i = row, j = column; j >= 0; i--, j--) {
			if (i < 0) {
				break;
			}
			
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[i][j].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(i, j));
				break;
			} // ...to here.
			nextMoves.add(new Place(i, j));
		}
		
		
		return nextMoves;
	}
	
	@Override
	public String toString()
	{
		return (isWhite?"wB":"bB");
	}
}









package Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * ChessPiece is the standard template for a chess piece. Specialized behavior is
 * implemented in the subclasses extending this one.
 *<p>
 * Parent class of Pawn, Knight, Bishop, Rook, Queen, King classes
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */
public abstract class ChessPiece {
	/**
	 * Color of the chess piece
	 */
	boolean isWhite;				        // white or black. Should this field be final?
	
	/**
	 * tracks whether the piece has been taken or not
	 */
	boolean isTaken = false;				// has the piece been captured or not
	
	/**
	 * tracks if the piece has moved or not
	 */
	boolean hasMoved = false;	
	
	/**
	 * List of all possible next moves
	 */
	List<Place> nextMoves = new ArrayList<Place>();  //for each possible location create a new Place

	
	/*
	   Method definitions
	*/
	/**
	 * Returns the color of the chess piece
	 * @return	color of the piece
	 */
	public boolean getIsWhite() {
		return isWhite;
	}
	
	/**
	 * Returns the value of isTaken boolean
	 * @return	the value of isTaken
	 */
	public boolean getTaken() {
		return isTaken;
	}
	
	/**
	 * Sets the value of isTaken
	 * @param	true or false - taken or not
	 */
	public void setTaken(boolean isTaken) {
		this.isTaken = isTaken;
	}
	
	//accessors of hasMoved field
	/**
	 * Return whether the piece has moved
	 * @return	whether the piece has moved or not
	 */
	public boolean getHasMoved() {
		return hasMoved;
	}
	
	public void setMoved() {
		hasMoved = true;
	}

	//public setTaken() {
	//	taken = true;
	//}

	//subsequent methods are implemented in subclasses
	/**
	 * Returns a list of all possible moves
	 * @param 	the current location of the piece
	 * @param	chess board
	 * @return	a list of Place objects
	 */
	public abstract List<Place> GenerateNextMoves(Place location, ChessBoard board);
}

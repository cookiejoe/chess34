package Objects;
import Control.DuplicateBoardException;
import Control.InvalidMoveException;

/**
 * A class which instantiates only one board in order to run the game
 * the ChessBoard class also presides over the pieces and moves, takes them etc.
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */
public class ChessBoard
{
	private Place[][] places = new Place[8][8];
	private ChessPiece[] pieces;
	
	// Static variable to make sure we only instantiate one board
	static boolean boardMade = false;
	
	// Constructor only allows one to be made
	public ChessBoard () throws DuplicateBoardException
	{
		if (boardMade)
			throw new DuplicateBoardException("Board already in play!");
		boardMade = true;
		
		setPieces();
	}
	
	// Constructors for debugging
	public ChessBoard(ChessPiece piece)
	{
		setPieces(piece);
	}
	
	// Places the pieces on the board
	private void setPieces ()
	{
		for (int r = 0; r < 8; r++)
		{
			for (int c = 0; c < 8; c++)
			{
				places[r][c] = new Place(r, c);
			}
		}
		
		places[0][0].setPiece(new Rook(true));
		places[0][7].setPiece(new Rook(true));
		places[0][1].setPiece(new Knight(true));
		places[0][6].setPiece(new Knight(true));
		places[0][2].setPiece(new Bishop(true));
		places[0][5].setPiece(new Bishop(true));
		places[0][3].setPiece(new Queen(true));
		places[0][4].setPiece(new King(true));
		
		
		places[7][0].setPiece(new Rook(false));
		places[7][7].setPiece(new Rook(false));
		places[7][1].setPiece(new Knight(false));
		places[7][6].setPiece(new Knight(false));
		places[7][2].setPiece(new Bishop(false));
		places[7][5].setPiece(new Bishop(false));
		places[7][3].setPiece(new Queen(false));
		places[7][4].setPiece(new King(false));
		
		for (int c = 0; c < 8; c++)
		{
			places[1][c].setPiece(new Pawn(true));
			places[6][c].setPiece(new Pawn(false));
		}
	}
	
	// Debug consructor helper which places one piece in the middle of the board
	private void setPieces (ChessPiece piece)
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				places[i][j] = new Place(i,j);
			}
		}
		
		places[3][3].setPiece(piece);
	}
	
	/* Set a place
	private void move (Place startPlace, int startCol, int startRow, Place endPlace, int endCol, int endRow)
	{
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				//System.out.println("Creating and setting places["+i+"]["+j+"].");
				places[i][j] = new Place(i,j);
			}
		}
		
		places[3][3].setPiece(piece);
	} */
	
	// Debug method to set text of a specific place
	public void setPlaceText (int row, int column, String text)
	{
		places[row][column].setText(text);
	}
	
	public void move (String start, String end) throws InvalidMoveException {
		// Get user move before entering here
		
		// Get the places on the board
		Integer[] startIndeces = Control.Chess.stringToIndex(start);
		Place startPlace = places[startIndeces[0]][startIndeces[1]];
		
		Integer[] endIndeces = Control.Chess.stringToIndex(end);
		Place endPlace = places[endIndeces[0]][endIndeces[1]];
		
		// Get the piece to move
		ChessPiece piece = startPlace.getPiece();
		
		// Get the piece to land on
		ChessPiece endPiece = endPlace.getPiece();
		
		// If there's no piece there, throw an exception!
		if (piece == null)
			throw new InvalidMoveException("No piece there bozo!");
		
		// If you're trying to take a piece, check for the color
		// if it's the same color as the other piece, give the player hell!
		// If they're on opposing teams, take it
		if (piece != null && endPiece != null)
			if (piece.getIsWhite() == endPiece.getIsWhite())
				throw new InvalidMoveException("You can't land on your own color dammit!");
			else
				take(endPlace, piece);
		
		// If the player needs to go back to chess school, give them more hell! Make Gordon Ramsey jealous!
		if (!piece.GenerateNextMoves(startPlace, this).contains(endPiece))
			throw new InvalidMoveException("This is not a valid move for this type of piece... Keep learning bozo!");
	
		// Actually move the piece
		startPlace.setPiece(null);
		if (endPiece != null)
			take(endPlace, piece);
		
		endPlace.setPiece(piece);
	}


	
	private void take(Place endPlace, ChessPiece piece) {
		// TODO Auto-generated method stub
		
	}

	public Place[][] getPlaces()
	{
		return places;
	}
	
	@Override
	public String toString()
	{
		StringBuilder boardBuilder = new StringBuilder();
		
		for (int r = 7; r >= 0; r--)
		{
			for (int c = 0; c <= 7; c++)
			{
				Place place = places[r][c];
				
				// If there's a piece, append the string representation to the string builder
				// Otherwise, append an empty square
				if (place.hasPiece())
				{
					boardBuilder.append(place.getPiece().toString());
				}
				else if (!place.getText().equals(""))
				{
					boardBuilder.append(place.getText());
				}
				else
				{
					boardBuilder.append(place.isWhite?"  ":"##");
				}
				
				boardBuilder.append(" ");
			}
			
			// Append the row number and a newline
			boardBuilder.append(" " + (r+1));
			boardBuilder.append('\n');
		}
		
		// Append a all the column letters
		boardBuilder.append(" a  b  c  d  e  f  g  h");
		boardBuilder.append('\n');
		
		return boardBuilder.toString();
	}
}

package Objects;

import java.util.List;

//import java.util.ArrayList;
/**
 * The Rook class implements abstract class ChessPiece 
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */
public class Rook extends ChessPiece {
	//fields are inherited from ChessPiece

	/**
	 * setter method for isWhite field in Rook
	 * @param isWhite
	 */
	public Rook (boolean isWhite) {
		this.isWhite = isWhite;
	}

	//these methods must be implemented
	/**
	 * Generate list of all possible moves.
	 * @return 	list of all possible moves
	 */
	public List<Place> GenerateNextMoves(Place location, ChessBoard board) {	//calculate all valid moves from current location
		/*
		bR bN bB bQ bK bB bN bR   8
		bp bp bp bp bp bp bp bp   7
		   ##    ##    ##    ##   6
		##    ##    ##    ##      5
		   ##    ##    ##    ##   4
		##    ##    ##    ##      3 
		wp wp wp wp wp wp wp wp   2
		wR wN wB wQ wK wB wN wR   1
		
		 a  b  c  d  e  f  g  h
		
		*  generate an array of places consisting of all the possible locations the piece may be 
		*  moved to on this particular turn. The controller will determine the subset of valid moves
		*/
		

		//A rook can move up-down column(traverse rows), left-right in row(traverse columns)
		//does the rook also need to know about castling?
		Place[][] places = board.getPlaces();
		
		/**
		 * row coordinate
		 */
		int row = location.getRowIndex();
		/**
		 * column coordinate
		 */
		int column = location.getColumnIndex();
		
		//rook/ forward-back-left-right movements
		//move forward
		for (int i = row; i < 8; i++) {
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[i][column].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(i, column));
				break;
			} // ...to here.
			nextMoves.add(new Place(i, column));
		}
		
		//move backward
		for (int i = row; i >= 0; i--) {
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[i][column].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(i, column));
				break;
			} // ...to here.
			nextMoves.add(new Place(i, column));
		}
		
		//move left
		for (int i = column; i >= 0; i--) {
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[row][i].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(row, i));
				break;
			} // ...to here.
			nextMoves.add(new Place(row, i));
		}
		
		//move right
		for (int i = column; i < 8; i++) {
			// Julian, this is your conscience speaking!
			// Copy from here...
			ChessPiece piece = places[row][i].getPiece();
			
			// If there is a piece in the way, add if it's enemy we can take, not if not, and break the loop.
			if (piece != null && piece != this)
			{
				if (piece.getIsWhite() == isWhite)
				{
					break;
				}
				nextMoves.add(new Place(row, i));
				break;
			} // ...to here.
			nextMoves.add(new Place(row, i));
		}
				
		
		return nextMoves;
	}
	
	@Override
	public String toString()
	{
		return (isWhite?"wR":"bR");
	}
}














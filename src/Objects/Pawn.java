package Objects;

import java.util.List;


/**
 * The Pawn class implements abstract class ChessPiece 
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */
public class Pawn extends ChessPiece {
	
	//fields unique to Pawn
	/**
	 * Track if the pawn is promotable or not
	 * 
	 */
	boolean isPromotable = false;
	
	public Pawn (boolean isWhite) {
		this.isWhite = isWhite;
	}
	
	/**
	 * Accessor method for field isPromotable
	 * @return  whether pawn is promotable 
	 */
	public boolean getIsPromotable() {
		return this.isPromotable;
	}
	
	/**
	 * Generate list of all possible moves.
	 * @return 	list of all possible moves
	 */
	public List<Place> GenerateNextMoves(Place location, ChessBoard board) {
		/*
		bR bN bB bQ bK bB bN bR   8
		bp bp bp bp bp bp bp bp   7
		   ##    ##    ##    ##   6
		##    ##    ##    ##      5
		   ##    ##    ##    ##   4
		##    ##    ##    ##      3 
		wp wp wp wp wp wp wp wp   2
		wR wN wB wQ wK wB wN wR   1
		
		 a  b  c  d  e  f  g  h
		
		*  generate an array of places consisting of all the possible locations the piece may be 
		*  moved to on this particular turn. The controller will determine the subset of valid moves
		*/
		Place[][] places = board.getPlaces();
		/**
		 * row coordinate
		 */
		int row = location.getRowIndex();
		/**
		 * column coordinate
		 */
		int column = location.getColumnIndex();
		ChessPiece piece;
		if (isWhite) {
			piece = places[row + 1][column].getPiece();		//check forward 1
			if (piece == null) {
				if (row == 7) {
					isPromotable = true;
				}
				nextMoves.add(new Place(row + 1, column));
				if (!hasMoved) {
					piece = places[row + 2][column].getPiece();		//check forward 2
					if (piece == null) {
						nextMoves.add(new Place(row + 2, column));
					}
				}
			}
		}
		else {
			piece = places[row - 1][column].getPiece();		//check forward 1
			if (piece == null) {
				nextMoves.add(new Place(row - 1, column));
				if (row == 2) {
					isPromotable = true;
				}
				if (!hasMoved) {
					piece = places[row - 2][column].getPiece();		//check forward 2
					if (piece == null) {
						nextMoves.add(new Place(row - 2, column));
					}
				}
			}
		}
		
		return nextMoves;
	}
	
	@Override
	public String toString()
	{
		return (isWhite?"wp":"bp");
	}
	
}

package Objects;

import java.util.List;


/**
 * The Knight class implements abstract class ChessPiece 
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */
public class Knight extends ChessPiece {
	//fields are inherited from ChessPiece

	public Knight (boolean isWhite) {
		this.isWhite = isWhite;
	}
	
	//these methods must be implemented
	/**
	 * Generate list of all possible moves.
	 * @return 	list of all possible moves
	 */
	public List<Place> GenerateNextMoves(Place location, ChessBoard board) {	//calculate all valid moves from current location
		/*
		bR bN bB bQ bK bB bN bR   8
		bp bp bp bp bp bp bp bp   7
		   ##    ##    ##    ##   6
		##    ##    ##    ##      5
		   ##    ##    ##    ##   4
		##    ##    ##    ##      3 
		wp wp wp wp wp wp wp wp   2
		wR wN wB wQ wK wB wN wR   1
		
		 a  b  c  d  e  f  g  h
		
		*  generate an array of places consisting of all the possible locations the piece may be 
		*  moved to on this particular turn. The controller will determine the subset of valid moves
		*/
		
		//like for the King and Queen, control for java.lang.ArrayIndexOutOfBoundsException s
		//A Knight moves in an L-Shape
		/*     o o 
		 *   o     o
		 *      x
		 *   o     o
		 *     o o
		 */
		/**
		 * row coordinate
		 */
		int row = location.getRowIndex();
		/**
		 * column coordinate
		 */
		int column = location.getColumnIndex();
		
		try {
			nextMoves.add(new Place(row + 2, column + 1));	//forward 2, right 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		}
		
		try {
			nextMoves.add(new Place(row + 2, column - 1));	//forward 2, left 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		}  
		
		try {
			nextMoves.add(new Place(row + 1, column + 2));	//forward 1, right 2
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		} 
		
		try {
			nextMoves.add(new Place(row + 1, column - 2));	//forward 1, left 2
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		} 
		
		try {
			nextMoves.add(new Place(row - 2, column + 1));	//backward 2, right 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		} 
		
		try {
			nextMoves.add(new Place(row - 2, column - 1));	//backward 2, left 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		} 
			
		try {
			nextMoves.add(new Place(row - 1, column + 2));	//backward 1, right 2
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		} 
		
		try {
			nextMoves.add(new Place(row - 1, column - 2));	//backward 1, left 2
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("Knight out of bounds error");
		} 
		
		
		return nextMoves;
	}
	
	@Override
	public String toString()
	{
		return (isWhite?"wN":"bN");
	}
}




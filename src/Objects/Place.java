package Objects;
/**
 * This class defines a square on the board, includes a spot for a ChessPiece, a toString and an equals
 * implementation which allows the piece to be identified with "a1" "b1" "c1" etc.
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */
public class Place {
	// The ChessPiece in this spot
	private ChessPiece piece = null;
	
	private String text = "";
	
	// The row and column index
	int row;
	int column;
	
	// Whether it's white or not
	boolean isWhite;
	
	public Place (String location)
	{
;
		
		// Get the indeces
		Integer[] indeces = Control.Chess.stringToIndex(location);
		
		row = indeces[0];
		column = indeces[1];
		
		if (row>7||column>7||row<0||column<0)
		{
			throw(new ArrayIndexOutOfBoundsException());
		}
		
		// If the indeces add up to an even number, it's a white space, if it's odd, it's a black space
		isWhite = (indeces[0]+indeces[1])%2 != 0;
	}
	
	public Place (int row, int column)
	{
		if (row>7||column>7||row<0||column<0)
		{
			throw(new ArrayIndexOutOfBoundsException());
		}
		
		// Get the ideces
		this.row = row;
		this.column = column;
		
		// If the indeces add up to an even number, it's a white space, if it's odd, it's a black space
		isWhite = (row+column)%2 != 0;
	}
	
	// Debug setter
	public void setText (String text)
	{
		this.text = text;
	}
	
	// This piece sets the piece, if it's null (no piece), it'll set it to null
	public void setPiece (ChessPiece piece)
	{
		this.piece = piece;
	}
	
	// Returns whether or not this place has a piece on it
	public boolean hasPiece ()
	{
		return !(piece == null);
	}
	
	// Get the piece in this spot
	public ChessPiece getPiece ()
	{
		return piece;
	}
	
	// Get the location of this place
	public String getText()
	{
		return text;
	}
	
	
	// Get row
	public int getRowIndex()
	{
		return row;
	}
	
	// Get column
	public int getColumnIndex()
	{
		return column;
	}
	
	@Override
	public String toString()
	{
		return "row:"+row+" column:"+column;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if (other instanceof Place &&
				((Place)other).getRowIndex() == (this.row) &&
				((Place)other).getColumnIndex() == (this.column) /*&&
				((Place)other).getText().equals(this.text)*/)
			return true;
		return false;
	}
}

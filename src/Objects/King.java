package Objects;

import java.util.List;

/**
 * The King class implements abstract class ChessPiece 
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 */

public class King extends ChessPiece {
	//fields are inherited from ChessPiece
	
	// special fields for King only
	/**
	 * tracks whether the King may castle or not
	 */
	boolean mayCastle = true;				// set to false if the King moves or if the King castles
	
	public King (boolean isWhite) {			//constructor
		this.isWhite = isWhite;
	}

	//these methods must be implemented
	/**
	 * Generate list of all possible moves.
	 * @return 	list of all possible moves
	 */
	public List<Place> GenerateNextMoves(Place location, ChessBoard board) {	//calculate all valid moves from current location
		/*
		bR bN bB bQ bK bB bN bR   8
		bp bp bp bp bp bp bp bp   7
		   ##    ##    ##    ##   6
		##    ##    ##    ##      5
		   ##    ##    ##    ##   4
		##    ##    ##    ##      3 
		wp wp wp wp wp wp wp wp   2
		wR wN wB wQ wK wB wN wR   1
		
		 a  b  c  d  e  f  g  h
		
		*  generate an array of places consisting of all the possible locations the piece may be 
		*  moved to on this particular turn. The controller will determine the subset of valid moves
		*/
		

		//A king can move one square in any direction (horizontally, vertically, or diagonally)
		/*
		 *    o o o
		 *    o x o
		 *    o o o
		 */
		//if mayCastle is true, generate locations for kingside and queenside castling 
		/**
		 * row coordinate
		 */
		int row = location.getRowIndex();
		/**
		 * column coordinate
		 */
		int column = location.getColumnIndex();
		
		//if at edge of board a java.lang.ArrayIndexOutOfBoundsException will be thrown for some of these
		//depending on where the King is located at the time
		//figure out how to best handle these
		
		try {
			nextMoves.add(new Place(row + 1, column));	//move forward 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
		
		try {
			nextMoves.add(new Place(row - 1, column));	//move backward 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
		
		try {
			nextMoves.add(new Place(row, column - 1));  //move left 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
			
		try {
			nextMoves.add(new Place(row, column + 1));  //move right 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
		
		try {
			nextMoves.add(new Place(row + 1, column - 1));  //move left 1, forward 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
		
		try {
			nextMoves.add(new Place(row - 1, column - 1));  //move left 1, backward 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
		
		try {
			nextMoves.add(new Place(row + 1, column + 1));  //move right 1, forward 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
		
		try {
			nextMoves.add(new Place(row - 1, column + 1));  //move right 1, backward 1
		} catch (ArrayIndexOutOfBoundsException e) {
			//System.err.println("King out of bounds error");
		}
			
		return nextMoves;
	}
	@Override
	public void setMoved() {
		hasMoved = true;
		mayCastle = false;
	}
	
	@Override
	public String toString()
	{
		return (isWhite?"wK":"bK");
	}
}






/*
 * Chess34, an ASCII 2-player chess program written in Java
 * By Julian Dauemer, Joe Buchoff
 * For CS213 at Rutgers University
 */

// Control package includes classes which relate to procedure and game-level methods
package Control;

import java.util.ArrayList;

import Objects.ChessBoard;
import Objects.ChessPiece;
import Objects.Place;

/**
 * Controller / driver of the Chess program
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 *
 */
public class Chess
{
	// Did the dude ask to draw?
	private static boolean drawFlag;
	
	// For fringe cases involving invalid moves during check and pawn promotion
	private static ChessPiece origStartPiece;
	
	// Declare our board
	private static Objects.ChessBoard battleground;
	
	// Is the game still playing? This variable will tell us
	private static boolean gameOn = true;
	
	// Boolean to hold which color's turn
	private static boolean isWhiteTurn = true;
	
	// Boolean to hold which color's turn
	private static boolean checkFlag = false;
	
	// Declare the indeces arrays
	private static Integer[] start;

	private static Integer[] end;
	
	// Array to hold the board
	private static Place[][] places;
	
	// Black King's place
	private static Place blackKing;
	
	// White King's place
	private static Place whiteKing;
	
	// Arraylist to hold possible next moves of a side in order to figure out Check
	private static ArrayList<Place> possibleNextMoves;
	
	// String to hold the user's command
	private static String command;
	
	public static void main (String[] args)
	{
		// Instantiate our board
		try {
			battleground = new Objects.ChessBoard();
		} catch (DuplicateBoardException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// Set our starting board
		places = battleground.getPlaces();
		
		// Initialize possibleNextMoves
		possibleNextMoves = new ArrayList<Place>();
				
		// Set king spots
		blackKing = places[7][4];
		whiteKing = places[0][4];
		
		// Declare startPlace and endPlace
		Objects.Place startPlace;
		Objects.Place endPlace;
		
		// Declare startPiece and endPiece
		ChessPiece startPiece;
		ChessPiece endPiece;
		
		// Command length - will be used to validate certain inputs
		int length;
		
		// Declare promotionPiece
		ChessPiece promotionPiece;
		
		// Char to hold the promotion piece wanted
		char promotionPieceChar;
		
		// Print the board
		System.out.println(battleground);
		
		// The main game loop
		while(gameOn)
		{
			// Update the local copy of the board
			places = battleground.getPlaces();
			
			// Reset promotionPieceChar
			promotionPieceChar = '\\';
			
			// Update checkFlag
			checkFlag = isInCheck(isWhiteTurn, battleground);
			
			// Get user input
			System.out.print((checkFlag?"Check ":"")+(isWhiteTurn?"White's ":"Black's ")+"move: ");
			command = terminalIO.getInput();
			
			// If the command is invalid, tell the user and get more input
			if (command == null)
			{
				System.out.println("\nIllegal move, try again\n");
				continue;
			} else {
				command = command.toLowerCase();
			}
			
			// Calculate length
			length = command.length();
			
			// Check for resign, draw
			if (command.equals("resign"))
			{
				resign();
				break;
			}
			// check for draw - if the last 5 characters of the input are "draw?"
			else if (length >=5 && command.substring(length - 5).equalsIgnoreCase("draw?"))
				drawFlag = true;
			
			if (length > 5)
			{
				promotionPieceChar = command.charAt(5);
			}
			// If the first player asked for a draw, check for agreement
			if (drawFlag)
			{
				if (command.equals("draw"))
					draw();
				else
					drawFlag = false;
			}
			
			else
			{	
				// Get the places and pieces
				start = stringToIndex(command.substring(0, 2));
				startPlace = battleground.getPlaces() [start[0]] [start[1]];
				
				end = stringToIndex(command.substring(3, 5));
				endPlace = battleground.getPlaces() [end[0]] [end[1]];
				
				origStartPiece = startPlace.getPiece();
				startPiece = origStartPiece;
				int startColumn = startPlace.getColumnIndex();
				int startRow = startPlace.getRowIndex();
				
				// If we're trying to move a piece that's not ours, tell the user and prompt again.
				if (startPiece.getIsWhite() != isWhiteTurn)
				{
					System.out.println("\nIllegal move, try again\n");
					continue;
				}
				
				endPiece = endPlace.getPiece();
				int endColumn = endPlace.getColumnIndex();
				int endRow = endPlace.getRowIndex();

				// If the pawn is trying to take a piece diagonally skip move()
				if (startPiece instanceof Objects.Pawn && Math.abs(endColumn-startColumn) == 1 &&
						endRow-startRow == (startPiece.getIsWhite()?1:-1) && endPiece != null &&
						endPiece.getIsWhite() != startPiece.getIsWhite())
				{
					
					// Actually move the piece, switch colors and go to the next turn.
					startPlace.setPiece(null);
					endPlace.setPiece(startPiece);
					
					// Make sure the move does not conflict with user being in check, i.e. was already
					// in check, and still is after this move
					if (checkFlag)
						if (isInCheck(isWhiteTurn, battleground))
						{
							unmove(startPlace,endPlace,origStartPiece,endPiece);
							System.out.println("\nIllegal move, try again\n");
							continue;
						}
					
					isWhiteTurn = !isWhiteTurn;
					continue;
				}
				
				// Try the move. If it's invalid, return to the beginning of the turn.
				try
				{
					move(startPlace, endPlace, promotionPieceChar);
				} catch (Exception e) {
					System.out.println(e.getMessage());
					continue;
				}
				// Make sure the user got themselves out of check. If not, unmove the pieces
				if (checkFlag)
					if (isInCheck(isWhiteTurn, battleground))
					{
						unmove(startPlace, endPlace, origStartPiece, endPiece);
						
						System.out.println("\nIllegal move, try again\n");
						continue;
					}
			}
			// Switch turns
			isWhiteTurn = !isWhiteTurn;
			
			// If I just put my opponent in checkmate, I've won the game!
			if (isInCheckMate(!isWhiteTurn, battleground))
			{
				win(isWhiteTurn);
				break;
			}
			
			// Print the board
			System.out.println();
			System.out.println(battleground);
		}
	}
	
	// Draw the game
	private static void draw() {
		// TODO Auto-generated method stub
		if (!drawFlag)
			throw (new InvalidMoveException("\nIllegal move, try again\n"));
		
		gameOn = false;
		System.out.println("Draw");
	}
	
	// Move the piece
	private static void move (Place startPlace, Place endPlace, char promotionPieceChar) throws InvalidMoveException
	{
		ChessPiece startPiece = startPlace.getPiece();
		int startColumn = startPlace.getColumnIndex();
		int startRow = startPlace.getRowIndex();
		
		ChessPiece endPiece = endPlace.getPiece();
		int endColumn = endPlace.getColumnIndex();
		int endRow = endPlace.getRowIndex();
		
		// If you're trying to take your own piece, throw an exception
		if(endPiece != null && startPiece.getIsWhite() == endPiece.getIsWhite())
			throw new InvalidMoveException("\nIllegal move, try again\n");
		
		// Check for promotion
		// Is it a pawn?
		if (startPiece instanceof Objects.Pawn)
		{
			// If it's trying to take a piece right in front of it, throw an exception
			if (endPiece != null)
			{
				throw(new InvalidMoveException("\nIllegal move, try again\n"));
			}
			
			// Is the promotion piece something other than '\\'?
			if (promotionPieceChar != '\\')
			{
			
			// Is the player trying to move it to the last row?
				if (endPlace.getRowIndex() == (startPiece.getIsWhite()?7:0))
				{
					switch (promotionPieceChar)
					{
						case 'N':
							startPiece = new Objects.Knight(startPiece.getIsWhite());
							break;
						case 'R':
							startPiece = new Objects.Rook(startPiece.getIsWhite());
							break;
						case 'B':
							startPiece = new Objects.Bishop(startPiece.getIsWhite());
							break;
						case 'Q':
	
						case '\\':
							startPiece = new Objects.Queen(startPiece.getIsWhite());
							break;
						default:
							// The promotion piece isn't valid. Throw an exception
							throw(new InvalidMoveException("\nIllegal move, try again\n"));
					}
				// It's not in the last row, throw an exception
				} else {
					throw (new InvalidMoveException("\nIllegal move, try again\n"));
				}
			}
		}
		if (promotionPieceChar != '\\' && !(startPiece instanceof Objects.Pawn))
		{
			throw (new InvalidMoveException("You can only promote a Pawn, silly!"));
		}
		// Castling
		// Is the user trying to move a king?
		if (startPiece instanceof Objects.King)
		{
			// User wants to castle
			if (startPlace.getColumnIndex() == 4 &&
					(endColumn == 1 || endColumn == 6))
			{
				// Which direction does the user want to castle?
				int sign = (startColumn - endColumn>0)?-1:1;
				if (startPiece.getIsWhite())
				{
					ChessPiece rook = battleground.getPlaces()[0][endColumn + sign].getPiece();
					
					// Have any pieces moved???
					if (!startPiece.getHasMoved() /* If the king hasn't yet moved */ &&
							/* If the piece in the relevant corner is a rook and hasn't moved */
							rook instanceof Objects.Rook &&
							!rook.getHasMoved())
					{
						// If there's a piece in the way, throw an exception
						for (int i = startColumn + sign; i < endColumn; i += sign)
						{
							if (places[startRow][i].getPiece() != null)
							{
								throw (new InvalidMoveException("\nIllegal move, try again\n"));
							}
						}
						
						// The king may no longer castle
						startPiece.setMoved();
						
						// The path is clear. Make the move and return
						
						// First move the king
						startPlace.setPiece(null);
						endPlace.setPiece(startPiece);
						
						// Now move the rook
						places[endRow][endColumn + sign].setPiece(null);
						places[endRow][endColumn - sign].setPiece(rook);
					} else {
						// One of the pieces already moved, don't allow the user to castle
						throw (new InvalidMoveException("\nIllegal move, try again\n"));
					}
				}
				
			// If the user is inputting an invalid move
			}
		}
		
		
		// If the move is invalid, throw an exception
		if (!startPiece.GenerateNextMoves(startPlace, battleground).contains(endPlace))
			throw new InvalidMoveException("\nIllegal move, try again\n");
		
		// Tell the program the piece has moved
		startPiece.setMoved();
		
		// Actually move the piece
		startPlace.setPiece(null);
		endPlace.setPiece(startPiece);
		
		//places[startRow][startColumn] = startPlace;
	}
	
	// Unmove the pieces
	private static void unmove (Place startPlace, Place endPlace, ChessPiece startPiece, ChessPiece endPiece)
	{
		startPlace.setPiece(startPiece);
		endPlace.setPiece(endPiece);
	}
			
	public static void resign ()
	{
		win (!isWhiteTurn);
	}
	
	public static void win (boolean isWhite)
	{
		System.out.println ((isWhite?"White":"Black")+" wins" );

		// End the game
		gameOn = false;
	}
	
	// Takes a String of a space and returns the index in places
	public static Integer[] stringToIndex (String place)
	{
		Integer[] indeces = {0, 0};
		
		// Get the letter from the String, convert it into an index
		char letter = place.charAt(0);
		
		switch (letter)
		{
			case 'a':
				indeces[1] = 0;
				break;
			case 'b':
				indeces[1] = 1;
				break;
			case 'c':
				indeces[1] = 2;
				break;
			case 'd':
				indeces[1] = 3;
				break;
			case 'e':
				indeces[1] = 4;
				break;
			case 'f':
				indeces[1] = 5;
				break;
			case 'g':
				indeces[1] = 6;
				break;
			case 'h':
				indeces[1] = 7;
				break;
			default: // If it's taken, for instance
				break;
		}
		
		// Get the number from the String, subtract 1 to turn it into an index
		indeces[0] = Integer.parseInt(place.substring(1)) - 1;
		
		return indeces;
	}
	
	// Checks if white (if true) black (if false) is in check
	public static boolean isInCheck (boolean isWhite, ChessBoard board)
	{
		Place[][] places = board.getPlaces();
		possibleNextMoves = new ArrayList<Place>();
		
		Place kingPlace = isWhite ? whiteKing : blackKing;
		
		// TODO check if the king is in check
		for (int r = 0; r < 8; r++)
		{
			for (int c = 0; c < 8; c++)
			{
				Place currPlace = places[r][c];
				ChessPiece piece = currPlace.getPiece();
				
				// If there is no piece in this Place, go to the next spot
				if (piece == null)
					break;

				// If it's an enemy piece, add its moves to the roster
				if (piece.getIsWhite() != isWhite)
					possibleNextMoves.addAll(piece.GenerateNextMoves(currPlace,battleground));
			}
		}
		
		// If the other side's next moves include the king's spot, it's in check
		return possibleNextMoves.contains(kingPlace);
	}
	
	static boolean isInCheckMate(boolean isWhite, ChessBoard board)
	{
		ArrayList<Place> startPlaces = new ArrayList<Place>();
		ArrayList<ArrayList<Place>> possibleNextMoves = new ArrayList<ArrayList<Place>>();
		
		// If it's not already in check, return false
		if (!isInCheck (isWhite, board))
			return false;
	
		// Get the king's place
		Place kingPlace = isWhite ? whiteKing : blackKing;
		
		// TODO check if the king is in check
		for (int r = 0; r < 8; r++)
		{
			for (int c = 0; c < 8; c++)
			{
				Place currPlace = places[r][c];
				ChessPiece piece = currPlace.getPiece();
					
				// If there is no piece in this Place, go to the next spot
				if (piece == null)
					break;
				
				// If it's an enemy piece, add its moves to the roster
				if (piece.getIsWhite() == isWhite)
				{
					startPlaces.add(currPlace);
					possibleNextMoves.add((ArrayList<Place>)currPlace.getPiece().GenerateNextMoves(currPlace, battleground));
				}
			}
		}

		// Test all the possible moves to see if they take it out of check
		int index = 0;
		for (Place startPlace : startPlaces)
		{
			ArrayList<Place> possibleIndividualNextMoves = possibleNextMoves.get(index);
			
			for (Place endPlace : possibleIndividualNextMoves)
			{
				// Create phantom boards
				ChessBoard phantomBoard = battleground;
				
				try
				{
					move(startPlace, endPlace, '\\');
					
					// If one of the moves takes the king out of check, return false
					if (!isInCheck(isWhite, phantomBoard))
						return false;
				} catch(Exception e){}
			
			}
			index ++;
		}
		
		return true;
	}
}
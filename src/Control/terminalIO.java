package Control;

import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * terminalIO handles terminal input validation
 * 
 * @author Joe Buchoff
 * @author Julian Daeumer
 *
 */
public class terminalIO {
	/**
	 * contains input whatever is entered by a user
	 */
	private static String inputString;
	
	/**
	 * getInput passes input to a validation method and the validated string is returned to the caller
	 * @return  validated input from terminal to caller
	 */
	public static String getInput()
	{
		/**
		 * Scanner reads input from terminal 
		 */
		Scanner inputScanner = new Scanner(System.in);
		inputScanner.useDelimiter("\n");
		

		//System.out.println("Enter input: ");
		while (inputScanner.hasNextLine()) {
			//System.out.println("Reading input. HasNextLine:"+inputScanner.hasNextLine());
			inputString = inputScanner.nextLine();
			inputString = inputString.trim();
			//inputString = inputString.toLowerCase();
			

			//validate input
			/**
			 * holds either a validated string or null
			 */
			String validInput = validateInput();
			if (!(validInput == null))
			{
				return validInput;
			} else {
				return null;
			}
		}
		return "";
		
	}
	/**
	 * performs input validation of input string
	 * @return valid string
	 */
	public static String validateInput() {
		/*
		 * the following forms are valid:
		 *
		 * form 1 - 2 valid substrings:
		 * 	"[location of piece to move] whitespace [destination]"
		 *
		 * form 2 - 3 valid substrings:
		 *  "[location of piece to move] whitespace [destination] whitespace [resign]"
		 *  
		 * forrm 4 - 3 valid substrings:
		 * "[location of piece to move] whitespace [destination] whitespace [promotion character]"
		 */
		/**
		 * regex patterns
		 */
		Pattern p1 = Pattern.compile("[a-h][0-8][\\s+][a-h][0-8]");
		Pattern p2 = Pattern.compile("resign");
		Pattern p3 = Pattern.compile("[a-h][0-8][\\s+][a-h][0-8][\\s+][NQRB]");
		Pattern p4 = Pattern.compile("[a-h][0-8][\\s+][a-h][0-8][\\s+]draw[?]");
		Pattern p5 = Pattern.compile("draw");

		/**
		 * regex matchers
		 */
		Matcher m1 = p1.matcher(inputString);
		Matcher m2 = p2.matcher(inputString);
		Matcher m3 = p3.matcher(inputString);
		Matcher m4 = p4.matcher(inputString);
		Matcher m5 = p5.matcher(inputString);
		
		if (m1.matches() || m2.matches() || m3.matches() || m4.matches() || m5.matches()) {
			return inputString;
		}
		else {
			return null;
		}
	
	}
	
}


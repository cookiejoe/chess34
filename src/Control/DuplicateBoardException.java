package Control;
/*
 * This exception is thrown when a chess move made is invalid for any reason, such as a same-color piece
 * already on that spot, or a move outside the range of said piece
 */
public class DuplicateBoardException extends Exception
{
	/**
	 * To ensure accurate deserialization
	 */
	private static final long serialVersionUID = 1L;

	public DuplicateBoardException()
	{
		super();
	}
	
	// Exception constructor with message
	public DuplicateBoardException(String message)
	{
		super(message);
	}
}
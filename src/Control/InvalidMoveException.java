package Control;
import java.lang.Exception;

/*
 * This exception is thrown when a chess move made is invalid for any reason, such as a same-color piece
 * already on that spot, or a move outside the range of said piece
 */
public class InvalidMoveException extends RuntimeException
{
	// Exception constructor with message
	public InvalidMoveException(String message)
	{
		super(message);
	}
}
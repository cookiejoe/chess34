import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class terminalIO {
	public static void main(String[] args) {

		Scanner inputScanner = new Scanner(System.in);
		inputScanner.useDelimiter("\n");
		String inputString;

		System.out.println("Enter input: ");
		while (inputScanner.hasNext()) {
			inputString = inputScanner.next();
			inputString = inputString.trim();
			inputString = inputString.toLowerCase();
			//System.out.println(inputString + "\n");

			//validate input
			validateInput(inputString);
		}
	}
	public static String validateInput(String input) {
		/*
		 * the following forms are valid:
		 *
		 * form 1 - 2 valid substrings:
		 * 	"[location of piece to move] whitespace [destination]"
		 *
		 * form 2 - 3 valid substrings:
		 *  "[location of piece to move] whitespace [destination] whitespace [resign]"
		 */
		Pattern p1 = Pattern.compile("[a-z][0-9][\\s+][a-z][0-9]");
		Pattern p2 = Pattern.compile("[a-z][0-9][\\s+][a-z][0-9][\\s+]resign");
		Pattern p3 = Pattern.compile("[a-z][0-9][\\s+][a-z][0-9][\\s+][a-z]");

		Matcher m1 = p1.matcher(input);
		Matcher m2 = p2.matcher(input);
		Matcher m3 = p3.matcher(input);

		if (m1.matches() || m2.matches() || m3.matches()) {
			return input;
		}
		else {
			System.out.println("string " + input + " is not valid\n");
			return null;
		}
	}
}

